import { Injectable } from '@angular/core';
import { Coche } from '../coches/coche';

@Injectable()
export class CocheService{
	public coleccion_coches:Array<Coche>;

	addCoche(nombre_coche:Coche):Array<Coche>{
		// this.coleccion_coches.push(nombre_coche);
		// this.coleccion_coches = [new Coche(nombre_coche.nombre, nombre_coche.caballaje, nombre_coche.color)];
		this.coleccion_coches.push(new Coche(nombre_coche.nombre, nombre_coche.caballaje, nombre_coche.color));
		return this.getCoche();
	}

	deleteRopa(index:number):Array<Coche>{
		this.coleccion_coches.splice(index, 1);
		return this.getCoche();
	}

	getCoche():Array<Coche>{
		return this.coleccion_coches;
	}
}