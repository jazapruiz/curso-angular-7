import { Component } from '@angular/core';
import { Coche } from './coche';
// import { CocheService } from '../services/coche.service';

@Component({
	selector: 'coches',
	templateUrl: './coches.component.html'
	// providers: [CocheService]
})
export class CochesComponent{
	public coche:Coche;
	public listado_coches:Array<Coche>;

	constructor(
		// private _cocheService:CocheService
	){
		this.coche = new Coche('', '', '');
		this.listado_coches = [new Coche('Seat Panda', '140', 'Rojo')];
	}

	ngOnInit(){
		
	}

	onSubmit(){
		// this._cocheService.addCoche(this.coche);
		// console.log(this._cocheService.getCoche());
		this.listado_coches.push(this.coche);
		this.coche = new Coche('', '', '');
	}
}