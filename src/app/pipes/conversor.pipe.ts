import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'conversor'})
export class ConversorPipe implements PipeTransform {
  transform(number1, number2):string {
    let value_one = parseInt(number1);
    let value_two = parseInt(number2);
    let result = "la multiplicación de " + value_one + "x" + value_two + "=" + (value_one*value_two);
    return result;
  }
}