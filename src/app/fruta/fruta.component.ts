import { Component } from '@angular/core';

@Component({
	selector: 'app-fruta',
	templateUrl: './fruta.component.html'
})
export class FrutaComponent{
	public nombre_componente = 'Componente de fruta';
	public listado_frutas = 'Naranja, Manzana, Pera y Sandia';

	public nombre:string;
	public edad:number;
	public mayorDeEdad:boolean;
	public trabajos:Array<string> = ['Carpintero', 'Albañil', 'Fontanero'];
	public trabajos2:Array<any> = ['Carpintero', 22, 'Fontanero'];
	comodin:any ;
	comodin2:any;

	constructor(){
		this.nombre = 'Alexander Zapata';
		this.edad = 26;
		this.mayorDeEdad = true;
		this.comodin = 'Cualquier cosa';
		this.comodin2 = 15495196;
		console.log(this.nombre);
	}

	ngOnInit(){
		this.cambiarNombre();
		console.log('Usted cambio el nombre: ' + this.nombre);
		console.log('El ngOnInit() se ejecuta justo despues del constructor');

		// Variables y alcance
		var uno = 8;
		var dos = 15;
		if (uno == 8) {
			let uno = 16; // let: funciona dentro de un bloque de código, función, método...
			var dos = 30;
			console.log("Valor de la variable 'uno': " + uno + " | Valor de la variable 'dos': " + dos);
		}
		console.log("Valor de la variable 'uno': " + uno + " | Valor de la variable 'dos': " + dos);
	}

	cambiarNombre(){
		this.nombre = 'José Zapata';
	}
}