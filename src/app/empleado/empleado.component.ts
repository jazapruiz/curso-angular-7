import { Component } from '@angular/core';
import { Empleado } from './empleado'

@Component({
	selector: 'app-empleado',
	templateUrl: './empleado.component.html'
})
export class EmpleadoComponent{
	public titulo = 'Componente empleados';
	public empleado:Empleado;
	public trabajadores:Array<Empleado>;
	public trabajadorExterno:boolean;
	public color:string;
	public color_seleccionado:string;

	constructor(){
		this.empleado = new Empleado('David Lopez', 45, 'Cocinero', true);
		this.trabajadores = [
			new Empleado('Alexander Zapata', 26, 'Programador', true),
			new Empleado('Luis Flores', 30, 'Cocinero', true),
			new Empleado('Victor Robles', 66, 'Barrendero', false)
		];
		this.trabajadorExterno = true;
		this.color = 'green';
		this.color_seleccionado = '#ccc';
	}

	ngOnInit(){
		console.log(this.empleado);
		console.log(this.trabajadores);
	}

	cambiarExterno(estado){
		this.trabajadorExterno = estado;
	}
}